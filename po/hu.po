# Hungarian translation of Sylpheed.
# Copyright (C) 2000 Free Software Foundation, Inc.
# Székely Kriszitán <szekelyk@different.hu>, 2001.
# Zahemszky Gábor <Gabor@Zahemszky.HU>, 2002, 2003, 2004.
# Németh Tamás <ntomasz@vipmail.hu>, 2005, 2006.
msgid ""
msgstr ""
"Project-Id-Version: Sylpheed-2.2.6\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2008-02-19 10:48+0900\n"
"PO-Revision-Date: 2006-07-15 17:27+0100\n"
"Last-Translator: Németh Tamás <ntomasz@vipmail.hu>\n"
"Language-Team: <NONE>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: libsylph/account.c:56
msgid "Reading all config for each account...\n"
msgstr "Beállítások beolvasása az összes hozzáféréshez...\n"

#: libsylph/imap.c:465
#, c-format
msgid "IMAP4 connection to %s has been disconnected. Reconnecting...\n"
msgstr "Az IMAP4 kapcsolat %s felé megszakadt. Újrakapcsolódás...\n"

#: libsylph/imap.c:520 libsylph/imap.c:526
msgid "IMAP4 server disables LOGIN.\n"
msgstr "IMAP szerver LOGIN kikapcsolva.\n"

#: libsylph/imap.c:602
#, c-format
msgid "creating IMAP4 connection to %s:%d ...\n"
msgstr "%s:%d IMAP4 kapcsolat létrehozása ...\n"

#: libsylph/imap.c:646
msgid "Can't start TLS session.\n"
msgstr "Nem lehet TLS menetet indítani.\n"

#: libsylph/imap.c:1120
#, c-format
msgid "Getting message %d"
msgstr "%d üzenet letöltése"

#: libsylph/imap.c:1236
#, c-format
msgid "Appending messages to %s (%d / %d)"
msgstr "Üzenetek hozzáfűzése ide: %s (%d / %d)"

#: libsylph/imap.c:1328
#, c-format
msgid "Moving messages %s to %s ..."
msgstr "%s üzenetek áthelyezése ide: %s ..."

#: libsylph/imap.c:1334
#, c-format
msgid "Copying messages %s to %s ..."
msgstr "%s üzenetek másolása ide: %s ..."

#: libsylph/imap.c:1473
#, c-format
msgid "Removing messages %s"
msgstr "%s üzenetek eltávolítása"

#: libsylph/imap.c:1479
#, c-format
msgid "can't set deleted flags: %s\n"
msgstr "nem lehet beállítani a törölt jelzést: %s\n"

#: libsylph/imap.c:1487 libsylph/imap.c:1582
msgid "can't expunge\n"
msgstr "nem lehet törölni\n"

#: libsylph/imap.c:1570
#, c-format
msgid "Removing all messages in %s"
msgstr "Minden üzenet eltávolítása ebből: %s?"

#: libsylph/imap.c:1576
msgid "can't set deleted flags: 1:*\n"
msgstr "nem lehet beállítani a törölt jelzést: 1:*\n"

#: libsylph/imap.c:1624
msgid "can't close folder\n"
msgstr "mappa bezárása nem sikerült\n"

#: libsylph/imap.c:1702
#, c-format
msgid "root folder %s not exist\n"
msgstr "%s root mappa nem létezik\n"

#: libsylph/imap.c:1891 libsylph/imap.c:1899
msgid "error occurred while getting LIST.\n"
msgstr "hiba történt a LIST lekérdezése közben.\n"

#: libsylph/imap.c:2013
#, c-format
msgid "Can't create '%s'\n"
msgstr "'%s' nem hozható létre\n"

#: libsylph/imap.c:2018
#, c-format
msgid "Can't create '%s' under INBOX\n"
msgstr "'%s' nem hozható létre a BEJÖVŐ mappa alatt\n"

#: libsylph/imap.c:2079
msgid "can't create mailbox: LIST failed\n"
msgstr "nem lehet létrehozni postaládát: LIST sikertelen\n"

#: libsylph/imap.c:2099
msgid "can't create mailbox\n"
msgstr "nem lehet létrehozni postaládát\n"

#: libsylph/imap.c:2203
#, c-format
msgid "can't rename mailbox: %s to %s\n"
msgstr "nem lehet a postaládát átnevezni: %s -> %s\n"

#: libsylph/imap.c:2283
msgid "can't delete mailbox\n"
msgstr "nem lehet törölni a postaládát\n"

#: libsylph/imap.c:2327
msgid "can't get envelope\n"
msgstr "a borítékot nem lehet megszerezni\n"

#: libsylph/imap.c:2340
#, c-format
msgid "Getting message headers (%d / %d)"
msgstr "Üzenetfejlécek letöltése (%d / %d)"

#: libsylph/imap.c:2350
msgid "error occurred while getting envelope.\n"
msgstr "hiba történt a boríték fogadása közben.\n"

#: libsylph/imap.c:2372
#, c-format
msgid "can't parse envelope: %s\n"
msgstr "nem lehet értelmezni a borítékot: %s\n"

#: libsylph/imap.c:2496
#, c-format
msgid "Can't connect to IMAP4 server: %s:%d\n"
msgstr "%s:%d IMAP4 szerverhez nem lehet kapcsolódni\n"

#: libsylph/imap.c:2503
#, c-format
msgid "Can't establish IMAP4 session with: %s:%d\n"
msgstr "IMAP4 szerverhez nem sikerült kapcsolódni: %s:%d\n"

#: libsylph/imap.c:2578
msgid "can't get namespace\n"
msgstr "névtér nem elérhető\n"

#: libsylph/imap.c:3111
#, c-format
msgid "can't select folder: %s\n"
msgstr "%s mappa kiválasztása nem sikerült\n"

#: libsylph/imap.c:3146
msgid "error on imap command: STATUS\n"
msgstr "hiba az IMAP parancs közben: STATUS\n"

#: libsylph/imap.c:3269 libsylph/imap.c:3304
msgid "IMAP4 authentication failed.\n"
msgstr "Az IMAP4 azonosítás nem sikerült.\n"

#: libsylph/imap.c:3353
msgid "IMAP4 login failed.\n"
msgstr "IMAP4 bejelentkezés sikertelen.\n"

#: libsylph/imap.c:3689
#, c-format
msgid "can't append %s to %s\n"
msgstr "%s nem fűzhető hozzá ehhez: %s\n"

#: libsylph/imap.c:3696
msgid "(sending file...)"
msgstr "(fájl küldése...)"

#: libsylph/imap.c:3725
#, c-format
msgid "can't append message to %s\n"
msgstr "üzenet nem fűzhető hozzá ehhez: %s\n"

#: libsylph/imap.c:3757
#, c-format
msgid "can't copy %s to %s\n"
msgstr "%s nem másolható ide: %s\n"

#: libsylph/imap.c:3781
#, c-format
msgid "error while imap command: STORE %s %s\n"
msgstr "hiba az IMAP parancs közben: STORE %s %s\n"

#: libsylph/imap.c:3795
msgid "error while imap command: EXPUNGE\n"
msgstr "hiba az IMAP parancs közben: EXPUNGE\n"

#: libsylph/imap.c:3808
msgid "error while imap command: CLOSE\n"
msgstr "hiba az IMAP parancs közben: CLOSE\n"

#: libsylph/imap.c:4084
#, c-format
msgid "iconv cannot convert UTF-7 to %s\n"
msgstr "az iconv nem tud UTF-7-ből konvertálni ebbe: %s\n"

#: libsylph/imap.c:4114
#, c-format
msgid "iconv cannot convert %s to UTF-7\n"
msgstr "az iconv nem tudja UTF-7-re konvertálni ezt: %s\n"

#: libsylph/mbox.c:50 libsylph/mbox.c:196
msgid "can't write to temporary file\n"
msgstr "nem lehet az ideiglenes fájlba írni\n"

#: libsylph/mbox.c:69
#, c-format
msgid "Getting messages from %s into %s...\n"
msgstr "Üzenetek fogadása innen: %s ide: %s...\n"

#: libsylph/mbox.c:79
msgid "can't read mbox file.\n"
msgstr "az mbox fájlt nem lehet olvasni.\n"

#: libsylph/mbox.c:86
#, c-format
msgid "invalid mbox format: %s\n"
msgstr "érvénytelen mbox formátum: %s\n"

#: libsylph/mbox.c:93
#, c-format
msgid "malformed mbox: %s\n"
msgstr "sérült mbox: %s\n"

#: libsylph/mbox.c:110
msgid "can't open temporary file\n"
msgstr "nem lehet ideiglenes fájlt megnyitni\n"

#: libsylph/mbox.c:161
#, c-format
msgid ""
"unescaped From found:\n"
"%s"
msgstr ""
"Kódolatlan feladó található:\n"
"%s"

#: libsylph/mbox.c:250
#, c-format
msgid "%d messages found.\n"
msgstr "%d üzenet található.\n"

#: libsylph/mbox.c:268
#, c-format
msgid "can't create lock file %s\n"
msgstr "%s zároló fájlt nem lehet létrehozni\n"

#: libsylph/mbox.c:269
msgid "use 'flock' instead of 'file' if possible.\n"
msgstr "'flock' használata 'file' helyett, ha lehet.\n"

#: libsylph/mbox.c:281
#, c-format
msgid "can't create %s\n"
msgstr "%s nem hozható létre\n"

#: libsylph/mbox.c:287
msgid "mailbox is owned by another process, waiting...\n"
msgstr "A postaláda egy másik processzhez tartozik, várakozás...\n"

#: libsylph/mbox.c:316
#, c-format
msgid "can't lock %s\n"
msgstr "%s nem zárolható\n"

#: libsylph/mbox.c:323 libsylph/mbox.c:373
msgid "invalid lock type\n"
msgstr "érvénytelen zároló típus\n"

#: libsylph/mbox.c:359
#, c-format
msgid "can't unlock %s\n"
msgstr "%s zárolása nem oldható fel\n"

#: libsylph/mbox.c:394
msgid "can't truncate mailbox to zero.\n"
msgstr "nem lehet nulla méretűre változtatni a postafiókot.\n"

#: libsylph/mbox.c:418
#, c-format
msgid "Exporting messages from %s into %s...\n"
msgstr "Üzenetek exportálása innen: %s ide: %s...\n"

#: libsylph/mh.c:427
#, c-format
msgid "can't copy message %s to %s\n"
msgstr "%s üzenet nem másolható ide: %s\n"

#: libsylph/mh.c:502 libsylph/mh.c:625
msgid "Can't open mark file.\n"
msgstr "A jelölő fájlt nem lehet megnyitni.\n"

#: libsylph/mh.c:509 libsylph/mh.c:631
msgid "the src folder is identical to the dest.\n"
msgstr "A forrás- és célmappa azonos.\n"

#: libsylph/mh.c:634
#, c-format
msgid "Copying message %s%c%d to %s ...\n"
msgstr "%s%c%d üzenet másolása ide: %s ...\n"

#: libsylph/mh.c:965 libsylph/mh.c:978
#, c-format
msgid ""
"File `%s' already exists.\n"
"Can't create folder."
msgstr ""
"`%s' fájl már létezik.\n"
"Mappa nem hozható létre."

#: libsylph/mh.c:1500
#, c-format
msgid ""
"Directory name\n"
"'%s' is not a valid UTF-8 string.\n"
"Maybe the locale encoding is used for filename.\n"
"If that is the case, you must set the following environmental variable\n"
"(see README for detail):\n"
"\n"
"\tG_FILENAME_ENCODING=@locale\n"
msgstr ""
"Könyvtár neve\n"
"A(z) '%s' nem valós UTF-8 sztring.\n"
"Lehetséges, hogy helyi karakterkódolást használ a fájlnevekre.\n"
"Ha ez a helyzet, akkor be kell állítania a következő környezeti változót\n"
"(lásd a README-t a részletekhez):\n"
"\n"
"\tG_FILENAME_ENCODING=@locale\n"

#: libsylph/news.c:207
#, c-format
msgid "creating NNTP connection to %s:%d ...\n"
msgstr "NNTP kapcsolat létesítése %s:%d ...\n"

#: libsylph/news.c:276
#, c-format
msgid "NNTP connection to %s:%d has been disconnected. Reconnecting...\n"
msgstr "Az NNTP kapcsolat %s:%d megszakadt. Újrakapcsolódás...\n"

#: libsylph/news.c:377
#, c-format
msgid "article %d has been already cached.\n"
msgstr "%d cikk már a gyorstárban van.\n"

#: libsylph/news.c:397
#, c-format
msgid "getting article %d...\n"
msgstr "%d cikk fogadása...\n"

#: libsylph/news.c:401
#, c-format
msgid "can't read article %d\n"
msgstr "%d cikk nem olvasható\n"

#: libsylph/news.c:676
msgid "can't post article.\n"
msgstr "A cikk nem küldhető el.\n"

#: libsylph/news.c:702
#, c-format
msgid "can't retrieve article %d\n"
msgstr "%d cikket nem lehet fogadni\n"

#: libsylph/news.c:759
#, c-format
msgid "can't select group: %s\n"
msgstr "%s csoport nem választható ki\n"

#: libsylph/news.c:796
#, c-format
msgid "invalid article range: %d - %d\n"
msgstr "érvénytelen cikk tartomány: %d - %d\n"

#: libsylph/news.c:809
msgid "no new articles.\n"
msgstr "Nincsenek új cikkek.\n"

#: libsylph/news.c:819
#, c-format
msgid "getting xover %d - %d in %s...\n"
msgstr "%d - %d hírjegyzék fogadása ide: %s...\n"

#: libsylph/news.c:823
msgid "can't get xover\n"
msgstr "A hírjegyzék nem elérhető\n"

#: libsylph/news.c:833
msgid "error occurred while getting xover.\n"
msgstr "Hiba a hírjegyzék fogadása közben.\n"

#: libsylph/news.c:843
#, c-format
msgid "invalid xover line: %s\n"
msgstr "érvénytelen hírjegyzék sor: %s\n"

#: libsylph/news.c:862 libsylph/news.c:894
msgid "can't get xhdr\n"
msgstr "xhdr nem elérhető\n"

#: libsylph/news.c:874 libsylph/news.c:906
msgid "error occurred while getting xhdr.\n"
msgstr "Hiba történt xhdr vétele közben.\n"

#: libsylph/nntp.c:68
#, c-format
msgid "Can't connect to NNTP server: %s:%d\n"
msgstr "%s:%d NNTP szerverhez nem sikerült kapcsolódni\n"

#: libsylph/nntp.c:164 libsylph/nntp.c:227
#, c-format
msgid "protocol error: %s\n"
msgstr "Protokollhiba: %s\n"

#: libsylph/nntp.c:187 libsylph/nntp.c:233
msgid "protocol error\n"
msgstr "Protokollhiba\n"

#: libsylph/nntp.c:283
msgid "Error occurred while posting\n"
msgstr "Hiba küldés közben\n"

#: libsylph/nntp.c:363
msgid "Error occurred while sending command\n"
msgstr "Hiba történt parancsküldés közben\n"

#: libsylph/pop.c:155
msgid "Required APOP timestamp not found in greeting\n"
msgstr "A szükséges APOP időpecsét nem található az üdvözletben\n"

#: libsylph/pop.c:162
msgid "Timestamp syntax error in greeting\n"
msgstr "Időpecsét szintaktikai hiba az üdvözletben\n"

#: libsylph/pop.c:170
#, fuzzy
msgid "Invalid timestamp in greeting\n"
msgstr "A szükséges APOP időpecsét nem található az üdvözletben\n"

#: libsylph/pop.c:198 libsylph/pop.c:225
msgid "POP3 protocol error\n"
msgstr "POP3 protokollhiba\n"

#: libsylph/pop.c:270
#, c-format
msgid "invalid UIDL response: %s\n"
msgstr "érvénytelen UIDL válasz: %s\n"

#: libsylph/pop.c:631
#, c-format
msgid "POP3: Deleting expired message %d\n"
msgstr "POP3: Lejárt üzenetek törlése %d\n"

#: libsylph/pop.c:640
#, c-format
msgid "POP3: Skipping message %d (%d bytes)\n"
msgstr "POP3: üzenet átugrása %d (%d bájt)\n"

#: libsylph/pop.c:673
msgid "mailbox is locked\n"
msgstr "a postafiók zárolt\n"

#: libsylph/pop.c:676
msgid "session timeout\n"
msgstr "kapcsolat időtúllépése\n"

#: libsylph/pop.c:682 libsylph/smtp.c:561
msgid "can't start TLS session\n"
msgstr "nem lehet TLS menetet indítani\n"

#: libsylph/pop.c:689 libsylph/smtp.c:496
msgid "error occurred on authentication\n"
msgstr "Hiba történt az azonosításkor\n"

#: libsylph/pop.c:694
msgid "command not supported\n"
msgstr "A parancs nem támogatott\n"

#: libsylph/pop.c:698
msgid "error occurred on POP3 session\n"
msgstr "hiba történt a POP3 kapcsolat közben\n"

#: libsylph/prefs.c:196 libsylph/prefs.c:224 libsylph/prefs.c:269
#: libsylph/prefs_account.c:217 libsylph/prefs_account.c:231
msgid "failed to write configuration to file\n"
msgstr "a beállításokat nem lehetett elmenteni\n"

#: libsylph/prefs.c:239
#, c-format
msgid "Found %s\n"
msgstr "Találat %s\n"

#: libsylph/prefs.c:272
msgid "Configuration is saved.\n"
msgstr "Beállítások tárolva.\n"

#: libsylph/prefs_common.c:503
#, fuzzy
msgid "Junk mail filter (manual)"
msgstr "Levélszemét szűrő"

#: libsylph/prefs_common.c:506
msgid "Junk mail filter"
msgstr "Levélszemét szűrő"

#: libsylph/procmime.c:1142
msgid "procmime_get_text_content(): Code conversion failed.\n"
msgstr "procmime_get_text_content(): Kód konverziós hiba.\n"

#: libsylph/procmsg.c:655
msgid "can't open mark file\n"
msgstr "A kijelölt fájl nem nyitható meg\n"

#: libsylph/procmsg.c:1107
#, c-format
msgid "can't fetch message %d\n"
msgstr "%d üzenet vétele nem sikerült\n"

#: libsylph/procmsg.c:1423
#, c-format
msgid "Print command line is invalid: `%s'\n"
msgstr "Érvénytelen nyomtató parancs: `%s'\n"

#: libsylph/recv.c:141
msgid "error occurred while retrieving data.\n"
msgstr "hiba az adatok fogadása közben.\n"

#: libsylph/recv.c:183 libsylph/recv.c:215 libsylph/recv.c:230
msgid "Can't write to file.\n"
msgstr "Nem lehet a fájlba írni.\n"

#: libsylph/smtp.c:157
msgid "SMTP AUTH not available\n"
msgstr "SMTP AUTH nem elérhető\n"

#: libsylph/smtp.c:466 libsylph/smtp.c:516
msgid "bad SMTP response\n"
msgstr "hibás SMTP válasz\n"

#: libsylph/smtp.c:487 libsylph/smtp.c:505 libsylph/smtp.c:602
msgid "error occurred on SMTP session\n"
msgstr "hiba történt az SMTP kapcsolat közben\n"

#: libsylph/ssl.c:54
msgid "SSLv23 not available\n"
msgstr "SSLv23 nem elérhető\n"

#: libsylph/ssl.c:56
msgid "SSLv23 available\n"
msgstr "SSLv23 elérhető\n"

#: libsylph/ssl.c:65
msgid "TLSv1 not available\n"
msgstr "TLSv1 nem elérhető\n"

#: libsylph/ssl.c:67
msgid "TLSv1 available\n"
msgstr "TLSv1 elérhető\n"

#: libsylph/ssl.c:101 libsylph/ssl.c:108
msgid "SSL method not available\n"
msgstr "SSL eljárás nem elérhető\n"

#: libsylph/ssl.c:114
msgid "Unknown SSL method *PROGRAM BUG*\n"
msgstr "Ismeretlen SSL eljárás *PROGRAM HIBA*\n"

#: libsylph/ssl.c:120
msgid "Error creating ssl context\n"
msgstr "Hiba az ssl környezet létrehozása közben\n"

#. Get the cipher
#: libsylph/ssl.c:139
#, c-format
msgid "SSL connection using %s\n"
msgstr "SSL kapcsolat %s használatával\n"

#: libsylph/ssl.c:148
msgid "Server certificate:\n"
msgstr "Szerver igazolvány:\n"

#: libsylph/ssl.c:151
#, c-format
msgid "  Subject: %s\n"
msgstr "  Tárgy: %s\n"

#: libsylph/ssl.c:156
#, c-format
msgid "  Issuer: %s\n"
msgstr "  Szerző: %s\n"

#: libsylph/utils.c:2682 libsylph/utils.c:2804
#, c-format
msgid "writing to %s failed.\n"
msgstr "%s fájlba nem sikerült írni.\n"

#~ msgid "can't change file mode\n"
#~ msgstr "fájl módja nem változtatható\n"
